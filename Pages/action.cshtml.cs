﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace brgvote.Pages
{
    public class ActionModel : PageModel
    {
        public string Message { get; set; }

        public void OnGet()
        {
            if (string.IsNullOrEmpty(Request.Query["login"]))
                return;
            var user = Program.Data.UserList.FindAll(p => p.Key.ToLower().Equals(((string)Request.Query["login"]).ToLower()));
            
            if (user.Count != 1)
            {
                Response.Redirect("/login");
                return;
            }

            if (!user.First().Name.Equals("admin"))
                user.First().loggedIn = true;
            
            HttpContext.Session.SetString("authenticatedUser", JsonConvert.SerializeObject(user.First()));
            Response.Redirect(user.First().Name == "admin" ? "/admin" : "/vote");
        }
    }
}