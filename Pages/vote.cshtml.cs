﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace brgvote.Pages
{
    public class VoteModel : PageModel
    {
        public bool AlreadyVoted { get; private set; }

        public void OnGet()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("authenticatedUser")))
            {
                Response.Redirect("/login");
                return;
            }

            Program.Data.UserList.FindAll(u =>
                    u.Key.Equals(JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser")).Key))
                .First().loggedIn = true;

            if (Program.Data.CurrentVote != null && Program.Data.CurrentVote.Votes
                    .FindAll(v => v.User.Key.Equals(JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser")).Key)).Count != 0)
            {
                AlreadyVoted = true;
                return;
            }

            if (Program.Data.CurrentVote != null && !string.IsNullOrEmpty(Request.Query["v"]))
            {
                if (Request.Query["v"].Equals("agree"))
                    Program.Data.CurrentVote.Votes.Add(new Vote(
                        Program.Data.UserList.FindAll(u =>
                                u.Key.Equals(JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser")).Key))
                            .First(),
                        VoteEnum.Agree));

                if (Request.Query["v"].Equals("disagree"))
                    Program.Data.CurrentVote.Votes.Add(new Vote(
                        Program.Data.UserList.FindAll(u =>
                            u.Key.Equals(JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser")).Key))
                            .First(),
                        VoteEnum.Disagree));

                if (Request.Query["v"].Equals("abstain"))
                    Program.Data.CurrentVote.Votes.Add(new Vote(
                        Program.Data.UserList.FindAll(u =>
                            u.Key.Equals(JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser")).Key))
                            .First(),
                        VoteEnum.Abstain));
            }

            if (Program.Data.CurrentTopic != null && !string.IsNullOrEmpty(Request.Query["action"]))
            {
                if (Request.Query["action"].Equals("newChangeProposal"))
                    Program.Data.CurrentTopic.Proposals.Add(new Proposal(ProposalEnum.Änderung, Request.Query["summary"],
                        JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser"))));

                if (Request.Query["action"].Equals("newExtensionProposal"))
                    Program.Data.CurrentTopic.Proposals.Add(new Proposal(ProposalEnum.Erweiterung, Request.Query["summary"],
                        JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser"))));
            }

            if (!string.IsNullOrEmpty(Request.Query["action"]))
            {
                if (Request.Query["action"].Equals("newSpeech"))
                    Program.Data.SpeechList.Add(new Speech(Program.Data.UserList.FindAll(u =>
                        u.Key.Equals(JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser")).Key))
                        .First(), Request.Query["summary"]));
            }

            if (!string.IsNullOrEmpty(Request.Query["v"]) || !string.IsNullOrEmpty(Request.Query["action"]))
            {
                Response.Redirect("/vote");
            }
        }
    }
}