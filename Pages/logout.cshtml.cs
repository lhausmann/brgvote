﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace brgvote.Pages
{
    public class LogoutModel : PageModel
    {
        public string Message { get; set; }

        public void OnGet()
        {
            HttpContext.Session.SetString("authenticatedUser", string.Empty);
            Response.Redirect("/login");
        }
    }
}