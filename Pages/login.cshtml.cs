﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace brgvote.Pages
{
    public class LoginModel : PageModel
    {
        public void OnGet()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("authenticatedUser")))
                Response.Redirect(HttpContext.Session.GetString("authenticatedUser") == "admin" ? "/admin" : "/vote");
        }
    }
}