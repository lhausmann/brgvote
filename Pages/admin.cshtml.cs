﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace brgvote.Pages
{
    public class AdminModel : PageModel
    {
        public void OnGet()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("authenticatedUser")) ||
                !JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser")).Name.Equals("admin"))
                Response.Redirect("/login");
            //TODO: MAKE THIS WORK System.IO.File.WriteAllText("data.json", JsonConvert.SerializeObject(Program.Data));
        }
    }
}