﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;

namespace brgvote
{
    public class Program
    {
        public static DataObj Data = new DataObj();
        
        public static void Main(string[] args)
        {
            //TODO: Make this work
            //if (File.Exists("data.json"))
            //{
            //    Data = JsonConvert.DeserializeObject<DataObj>(File.ReadAllText("data.json"));
            //}
            var studentsImport = StudentsImport.FromJson(File.ReadAllText("students.json"));
            foreach (var s in studentsImport)
            {
                Data.UserList.Add(new User((s.Vorname + " " + s.Nachname).Trim(), s.Klasse, s.Key));
            }
            
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")!= null && Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").Equals("Development"))
            {
                BuildDevelopmentWebHost(args).Run();
            }
            else
            {
                try
                {
                    if (args.Length != 1 || int.Parse(args[0]) > 65535 || int.Parse(args[0]) < 1)
                        throw new ArgumentException();
                }
                catch (Exception)
                {
                    Console.WriteLine("Compiled usage: ./program <port>");
                    Console.WriteLine("Debug usage: dotnet run <port>");
                    Environment.Exit(1);
                }

                BuildWebHost(args).Run();
            }
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .UseUrls("http://0.0.0.0:" + args[0])
                .Build();
        }

        public static IWebHost BuildDevelopmentWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .UseUrls("http://0.0.0.0:1234")
                .Build();
        }
    }


    public class DataObj
    {
        public Topic CurrentTopic;
        public Votable CurrentVote;
        public List<Speech> SpeechList = new List<Speech>();
        public List<Topic> TopicList = new List<Topic>();
        public List<User> UserList = new List<User>();
    }

    public class Topic : Votable
    {
        public List<Proposal> Proposals = new List<Proposal>();

        public Topic(string title)
        {
            Title = title;
        }
    }

    public class Proposal : Votable
    {
        public ProposalEnum Type { get; }
        public User User { get; }
        
        public Proposal(ProposalEnum type, string title, User user)
        {
            Title = title;
            Type = type;
            User = user;
        }
    }

    public enum ProposalEnum
    {
        Erweiterung,
        Änderung
    }

    public class Votable
    {
        private VotingStateEnum _votingStatus = VotingStateEnum.Idle;
        public string Title;
        public List<Vote> Votes = new List<Vote>();

        public void StartVote()
        {
            if (Program.Data.CurrentVote != null)
                return;
            _votingStatus = VotingStateEnum.Voting;
            Program.Data.CurrentVote = this;
        }

        public void EndVote()
        {
            if (Program.Data.CurrentVote == null)
                return;
            _votingStatus = VotingStateEnum.Ended;
            Program.Data.CurrentVote = null;
        }

        public VotingStateEnum GetVoteStatus()
        {
            return _votingStatus;
        }

        public int getVoteCount(VoteEnum e)
        {
            return Votes.FindAll(p => p.VoteEnum == e).Count;
        }

        public VoteResultEnum getVoteResult()
        {
            var agreeVotes = getVoteCount(VoteEnum.Agree);
            var disagreeVotes = getVoteCount(VoteEnum.Disagree);
            var abstainVotes = getVoteCount(VoteEnum.Abstain);

            if (Votes.Count < (Program.Data.UserList.Count - 1) / 3 * 2)
                return VoteResultEnum.ZuWenigStimmen;
            
            if (agreeVotes > disagreeVotes)
                return VoteResultEnum.Angenommen;
            if (agreeVotes < disagreeVotes)
                return VoteResultEnum.Abgelehnt;
            return VoteResultEnum.Gleichstand;
        }
        
        public VoteResultEnum getAltVoteResult()
        {
            var agreeVotes = getVoteCount(VoteEnum.Agree);
            var disagreeVotes = getVoteCount(VoteEnum.Disagree);
            var abstainVotes = getVoteCount(VoteEnum.Abstain);

            if (agreeVotes > disagreeVotes)
                return VoteResultEnum.Angenommen;
            if (agreeVotes < disagreeVotes)
                return VoteResultEnum.Abgelehnt;
            return VoteResultEnum.Gleichstand;
        }

        public string getVoteResultStr()
        {
            return getVoteResult() == VoteResultEnum.ZuWenigStimmen ? $"{getVoteResult()} ({getAltVoteResult()}) {getVoteCount(VoteEnum.Agree)}/{getVoteCount(VoteEnum.Disagree)}/{getVoteCount(VoteEnum.Abstain)}" : $"{getVoteResult()} {getVoteCount(VoteEnum.Agree)}/{getVoteCount(VoteEnum.Disagree)}/{getVoteCount(VoteEnum.Abstain)}";
        }
    }

    public enum VotingStateEnum
    {
        Idle,
        Voting,
        Ended
    }

    public enum VoteResultEnum
    {
        Angenommen,
        Abgelehnt,
        Gleichstand,
        ZuWenigStimmen
    }

    public class Speech
    {
        public Speech(User user, string summary)
        {
            User = user;
            Summary = summary;
        }

        public User User { get; }
        public string Summary { get; }
        public bool complete { get; set; } = false;
    }

    public class Vote
    {
        public User User;
        public VoteEnum VoteEnum;

        public Vote(User user, VoteEnum voteEnum)
        {
            VoteEnum = voteEnum;
            User = user;
        }
    }

    public enum VoteEnum
    {
        Agree,
        Disagree,
        Abstain
    }

    public class User
    {
        public User(string name, string @class, string key)
        {
            Name = name;
            Class = @class;
            Key = key;
        }

        public string Name { get; }
        public string Class { get; }
        public string Key { get; }
        public bool loggedIn { get; set; } = false;
    }
}