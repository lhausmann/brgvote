﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace brgvote.Pages
{
    public class IndexModel : PageModel
    {
        public void OnGet()
        {
            if (!string.IsNullOrEmpty(HttpContext.Session.GetString("authenticatedUser")))
                //Response.Redirect(HttpContext.Session.GetString("authenticatedUser") == "admin" ? "/config" : "/vote");
                Response.Redirect((JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser")).Name) == "admin" ? "/admin" : "/vote");
            else
                Response.Redirect("/login");
        }
    }
}