﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace brgvote.Pages
{
    public class ConfigModel : PageModel
    {
        public void OnGet()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString("authenticatedUser")) ||
                !JsonConvert.DeserializeObject<User>(HttpContext.Session.GetString("authenticatedUser")).Name.Equals("admin"))

            {
                Response.Redirect("/login");
                return;
            }

            if (string.IsNullOrEmpty(Request.Query["action"])) return;
            if (Request.Query["action"].Equals("nextTopic"))
            {
                foreach (var user in Program.Data.UserList)
                {
                    user.loggedIn = false;
                }
                if (Program.Data.CurrentTopic == null)
                {
                    Program.Data.CurrentTopic = Program.Data.TopicList.FirstOrDefault();
                }
                else if (Program.Data.TopicList.FindIndex(t => t.Title.Equals(Program.Data.CurrentTopic.Title)) ==
                         Program.Data.TopicList.Count - 1) { }
                else
                {
                    Program.Data.CurrentTopic = Program.Data.TopicList[
                        Program.Data.TopicList.FindIndex(t => t.Title.Equals(Program.Data.CurrentTopic.Title)) + 1];
                }
            }
            
            if (Request.Query["action"].Equals("starttopicvote"))
            {
                Program.Data.CurrentTopic.StartVote();
                Response.Redirect("/admin");
                return;
            }

            if (Request.Query["action"].Equals("endtopicvote"))
            {
                Program.Data.CurrentTopic.EndVote();
                Response.Redirect("/admin");
                return;
            }

            if (Request.Query["action"].Equals("startvote"))
            {
                Program.Data.CurrentTopic.Proposals[int.Parse(Request.Query["index"])].StartVote();
                Response.Redirect("/admin");
                return;
            }

            if (Request.Query["action"].Equals("endvote"))
            {
                Program.Data.CurrentTopic.Proposals[int.Parse(Request.Query["index"])].EndVote();
                Response.Redirect("/admin");
                return;
            }

            if (Request.Query["action"].Equals("dismissSpeech"))
            {
                Program.Data.SpeechList[int.Parse(Request.Query["index"])].complete = true;
                Response.Redirect("/admin");
                return;
            }

            if (Request.Query["action"].Equals("addTopic"))
                Program.Data.TopicList.Add(new Topic(Request.Query["topicName"]));

            Response.Redirect("/admin");
        }
    }
}