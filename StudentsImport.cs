﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using brgvote;
//
//    var studentsImport = StudentsImport.FromJson(jsonString);

namespace brgvote
{
    using System.Collections.Generic;
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using J = Newtonsoft.Json.JsonPropertyAttribute;

    public partial class StudentsImport
    {
        [J("Klasse")]   public string Klasse { get; set; }  
        [J("Nachname")] public string Nachname { get; set; }
        [J("Vorname")]  public string Vorname { get; set; } 
        [J("Key")]      public string Key { get; set; }     
    }

    public partial class StudentsImport
    {
        public static List<StudentsImport> FromJson(string json) => JsonConvert.DeserializeObject<List<StudentsImport>>(json, brgvote.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this List<StudentsImport> self) => JsonConvert.SerializeObject(self, brgvote.Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = { 
                new IsoDateTimeConverter()
                {
                    DateTimeStyles = DateTimeStyles.AssumeUniversal,
                },
            },
        };
    }
}
